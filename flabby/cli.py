#!/bin/env python3
"""Detect and report flabby words and phrases

  Command line interface to flabby utility

"""

import sys

import flabby

sys.exit(flabby.main(sys.argv))

