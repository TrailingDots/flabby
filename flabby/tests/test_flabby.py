"""

Unit tests to determine if everything works as expected.

Most (all?) unit tests has a line at the top that can be
used to execute that test. For development or maintenance
this offers a most convenient use.
"""

import unittest
import os
import pdb
import re
import readline # for debugging
import sys

# For debugging purposes
import pprint
pp = pprint.PrettyPrinter(indent=2)

sys.path.append("./flabby")
sys.path.append("./flabby/flabby")
import flabby
import flabby.flabby

from flabby.flabby import ALine, Flabby, PhraseEntry, ExtractPhrase

BAD_PHRASES_DIR = "./flabby/tests/"

def get_flabs_list(user_argv):
    (user_arg_dict, parser) = flabby.flabby.build_user_arg_dict(user_argv)
    flab = Flabby(user_arg_dict, parser)
    flabs_list = flab.flab_detector()
    return flabs_list

class TestFlabby(unittest.TestCase):
    def test_invalid_output(self):
        """
        Test the --output to a tmp file
        """
        user_argv = ["flabby.py",
                "--output", "a/b/c/d"  # bogus output file
            ]
        try:
            (_user_arg_dict, _parser) = flabby.flabby.build_user_arg_dict(user_argv)
        except:
            self.assertTrue(True)

    def test_redirect(self):
        """
        Test the --output to a tmp file
        """
        import contextlib
        user_argv = ["flabby.py",
                "--input", "./flabby/tests/about_data.txt",
                "--output", "/tmp/flabby.out"
            ]
        (user_arg_dict, parser) = flabby.flabby.build_user_arg_dict(user_argv)
        flab = Flabby(user_arg_dict, parser)
        flabs_list = flab.flab_detector()

        with open(user_arg_dict["output"], "w") as f:
                with contextlib.redirect_stdout(f):
                    flab.output_flabby_list(flabs_list)

    def test_get_user_text_input(self):
        """Verify that a dataset can load"""
        user_argv = ["flabby.py",
            "--input", "./flabby/tests/about_data.txt"
        ]
        #
        # Get down to the list of flabbies
        flabs_list = get_flabs_list(user_argv)

    def test_phrase_0_entries(self):
        """
        The PhraseEntry must properly parse the phrases entries.
        PhraseEntry gets used in building the flabby system dictionary.

        Does not have an Ex:Better - illegal, broken phrases input!
        """
        phrase = """Are after - Clunky verb construction. Use follow, or seek, or desire, or want."""
        dict_entry = PhraseEntry(phrase)
        self.assertEqual(dict_entry.aline,phrase)
        self.assertEqual(dict_entry.key,'are after')
        self.assertEqual(dict_entry.reason, \
            'Clunky verb construction. Use follow, or seek, or desire, or want')
        self.assertEqual(dict_entry.ex_better_list, [])


class TestPhraseEntry(unittest.TestCase):

    def test_phrase_entry_repr(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_phrase_entry_repr

        Test the repr() method of PhraseEntry

        The PhraseEntry must properly parse the phrases entries.
        PhraseEntry gets used in building the flabby system dictionary.

        Does not have an Ex:Better - illegal, broken phrases input!
        """
        phrase = """Are after - Clunky verb construction. Use follow, or seek, or desire, or want."""
        dict_entry = PhraseEntry(phrase)
        repr_str = repr(dict_entry)
        self.assertTrue(repr_str.find("key=are after", 0) != -1)
        self.assertTrue(repr_str.find("reason=Clunky", 0) != -1)
        self.assertTrue(repr_str.find("aline=Are after", 0) != -1)

    def test_phrase_1_entry(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_phrase_1_entry

        The PhraseEntry must properly parse the phrases entries.
        PhraseEntry gets used in building the flabby system dictionary.

        Uses a single Ex:Better:
        """
        phrase = """Are after - Clunky verb construction. Use follow, or seek, or desire, or want. Ex: The events are after the lecture. Better: The events follow the lecture."""
        dict_entry = PhraseEntry(phrase)
        self.assertEqual(dict_entry.aline, phrase)
        self.assertEqual(dict_entry.key, 'are after')
        self.assertEqual(dict_entry.reason, \
            'Clunky verb construction. Use follow, or seek, or desire, or want.')
        self.assertEqual(dict_entry.ex_better_list,
            [('The events are after the lecture.', 
                'The events follow the lecture.')])

    def test_phrase_2_entries(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_phrase_2_entries

        The PhraseEntry must properly parse the phrases entries.
        PhraseEntry gets used in building the flabby system dictionary.
        """
        phrase = """Are after - Clunky verb construction. Use follow, or seek, or desire, or want. Ex: The events are after the lecture. Better: The events follow the lecture. Ex: I don't know what you are after. Better: I don't know what you want.
        """
        dict_entry = PhraseEntry(phrase)
        self.assertEqual(dict_entry.aline, phrase)
        self.assertEqual(dict_entry.key, 'are after')
        self.assertEqual(dict_entry.reason, \
            'Clunky verb construction. Use follow, or seek, or desire, or want.')
        self.assertEqual(dict_entry.ex_better_list,
                [('The events are after the lecture.', 
                    'The events follow the lecture.'),
                 ("I don't know what you are after.", 
                    "I don't know what you want.")])

    def test_phrase_ex_no_better(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_phrase_ex_no_better

        The PhraseEntry must properly parse the phrases entries.
        PhraseEntry gets used in building the flabby system dictionary.

        Input has Ex: but no Better:  Illegal phrase input.
        """
        phrase = """Are after - Clunky verb construction. Use follow, or seek, or desire, or want. Ex: The events are after the lecture."""
        dict_entry = PhraseEntry(phrase)
        self.assertEqual(dict_entry.aline, phrase)
        self.assertEqual(dict_entry.key, 'are after')
        self.assertEqual(dict_entry.reason, \
            'Clunky verb construction. Use follow, or seek, or desire, or want.')
        self.assertEqual(dict_entry.ex_better_list, [])


    def test_phrase_better_no_ex(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_phrase_better_no_ex
        python -m unittest flabby.tests.test_flabby.TestPhraseEntry.test_phrase_better_no_ex

        The PhraseEntry must properly parse the phrases entries.
        PhraseEntry gets used in building the flabby system dictionary.

        Input has Better: but no Ex:  Illegal phrase input.
        """
        phrase = """Are after - Clunky verb construction. Use follow, or seek, or desire, or want. Better: The events are after the lecture."""
        dict_entry = PhraseEntry(phrase)
        # Does not parse well...
        self.assertEqual(dict_entry.aline, phrase)
        self.assertEqual(dict_entry.key, 'are after')
        self.assertEqual(dict_entry.reason, \
            'Clunky verb construction. Use follow, or seek, or desire, or want. Better: The events are after the lecture')
        self.assertEqual(dict_entry.ex_better_list, [])


    def test_phrase_no_reason(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.PhraseEntry.test_phrase_no_reason

        The PhraseEntry must properly parse the phrases entries.
        PhraseEntry gets used in building the flabby system dictionary.

        Input has no reason or Ex: or Better:. Illegal.
        """
        phrase = """Are after - """
        dict_entry = PhraseEntry(phrase)
        self.assertEqual(dict_entry.aline, phrase)
        self.assertEqual(dict_entry.key, 'are after')
        self.assertEqual(dict_entry.reason, '')
        self.assertEqual(dict_entry.ex_better_list, [])

    def test_scan_for_flab_1(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_scan_for_flab_1

        Test scan_for_flab()
        """

        user_argv = ["flabby.py",
            "--input", "./flabby/tests/bad_phrases"
        ]
        (user_arg_dict, parser) = flabby.flabby.build_user_arg_dict(user_argv)
        flab = Flabby(user_arg_dict, parser)
        flabs_list = flab.flab_detector()

    def test_scan_for_invalid_input_file(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_scan_for_invalid_input_file

        Test scan_for_flab()
        """

        user_argv = ["flabby.py",
            "--input", "./flabby/tests/bogus_input_file"
        ]
        status = flabby.flabby.main(user_argv)
        self.assertEqual(status, False)

    def test_scan_for_invalid_output_file(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestPhraseEntry.test_scan_for_invalid_output_file

        Test scan_for_flab()
        """

        user_argv = ["flabby.py",
            "--input", "README.md",
            "--output", "./a/b/c/d/bogus.txt"
        ]
        status = flabby.flabby.main(user_argv)
        self.assertEqual(status, False)

        
class TestExtractPhrase(unittest.TestCase):
    """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase
    """

    def test_extract_phrase_self(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_self
        """
        text = "Also so rare"
        aline = ALine(1, text, "")
        checker = ExtractPhrase(aline, "also")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 0)

        # Exercise the repr() for coverage
        repr(checker)


    def test_extract_phrase_good0(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_good0
        Success in finding phrase
        """

        aline = ALine(1, "Also so rate", "")
        key = "so"
        checker = ExtractPhrase(aline, key)
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 5)

    def test_extract_phrase_good1(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_good1
        Success in finding phrase
        """
        aline = ALine(1, "So, so rate", "")
        checker = ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 0)


    def test_extract_phrase_non_exist(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_non_exist
        Success in finding phrase
        """
        aline = ALine(1, "So, so rare", "")
        checker = flabby.flabby.ExtractPhrase(aline, "foobar")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_no_so_trailing(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_no_so_trailing
        """
        aline = ALine(1, "Great Software", "")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_phrase_empty_string(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_empty_string
        """
        aline = ALine(1, "", "")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_so_at_eol(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_so_at_eol
        """
        aline = ALine(1, "And so", "")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 4)

    def test_extract_so_in_middle0(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_so_in_middle0
        """
        aline = ALine(1, "also xxxsoyyy", "")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_so_in_middle1(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_so_in_middle1
        """
        aline = ALine(1, "also soyyy", "")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_so_in_middle2(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_so_in_middle2
        """
        aline = ALine(1, "also xxxso", "")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_so_on_2nd_line(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_so_on_2nd_line
        """
        aline = ALine(1, "And ", "so")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_so_embedded(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_so_embedded
        """
        aline = ALine(1, "debugger personalized for YOU? ", "so")
        checker = flabby.flabby.ExtractPhrase(aline, "so")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_phrase_on_line2(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_on_line2
        """
        aline = ALine(1, "And ", "bald-headed")
        checker = flabby.flabby.ExtractPhrase(aline, "bald-headed")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_split_word_phrase_on_2nd_line(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_split_word_phrase_on_2nd_line
        """
        aline = ALine(1, "People are", "after something.")
        # Phrase does a line wrap. This should work as the phrse
        # starts on the first line.
        checker = flabby.flabby.ExtractPhrase(aline, "are after")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 7)

    def test_extract_multi_word_phrase(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_multi_word_phrase
        """
        aline = ALine(1, "Are after something that", "goes on")
        checker = flabby.flabby.ExtractPhrase(aline, "are after")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 0)

    def test_extract_phrase_empty_key(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_empty_key
        """
        aline = ALine(1, "something X", "")
        checker = flabby.flabby.ExtractPhrase(aline, "")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_extract_phrase_key_with_punct(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_key_with_punct
        """
        aline = ALine(1, "He was a bald-headed man", "and so what")
        checker = flabby.flabby.ExtractPhrase(aline, "bald-headed")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 9)

    def test_extract_phrase_etc(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_phrase_etc
        """
        # Middle of string
        aline = ALine(1, "He etc. was a bald-headed man", "etc. on 2nd line")
        checker = ExtractPhrase(aline, "etc.")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 3)

    def test_extract_etc(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_extract_etc
        """
        # End of string
        aline = ALine(1, "He etc.", "and etc. on second line")
        checker = flabby.flabby.ExtractPhrase(aline, "etc.")
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 3)

    def test_embedded_get(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_embedded_get
        """
        aline = ALine(1, "pagetitle: Ideas for Courses Notes\nhtml: pandoc -s --toc ideas.md -o ideas.html", " get title not in 1st line")
        key = "get"
        checker = ExtractPhrase(aline, key)
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, -1)

    def test_embedded_square_brackets(self):
        """
        ipython -m unittest -- flabby.tests.test_flabby.TestExtractPhrase.test_embedded_square_brackets
        """
        aline = ALine(1, "and [sic], this ...", " ")
        key = "sic"
        checker = ExtractPhrase(aline, key)
        ndx = checker.extract_phrase()
        self.assertEqual(ndx, 5)


if __name__ == "__main__":
    unittest.main()

