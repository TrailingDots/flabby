#!/bin/env python3
"""

Detect and report flabby words and phrases.

This code will detect and report over 300
words and phrases that impact your writing
by suggesting rewording of the sharpness
of your compositions.

"""

from collections import namedtuple
import argparse
import os
import pdb
import pprint
import re
import sys

pp = pprint.PrettyPrinter(indent=2)

UserText = namedtuple("UserText", ['key', 'aline'])

def _flatten(list_of_lists):
    """
    Flatten a list of lists:
        [[1,2,3], [4,5,6]]
    returns:
        [1,2,3,4,5,6]

    While this method of concatenating lists can
    be slow, the lists tend to be short.
    """
    if len(list_of_lists) == 0:
        return list_of_lists
    if isinstance(list_of_lists[0], list):
        return _flatten(list_of_lists[0]) + _flatten(list_of_lists[1:])
    return list_of_lists[:1] + _flatten(list_of_lists[1:])


class ExtractPhrase():
    """
    ExtractPhrases will extract phrases from lines
    of text. Standard string routines linke "xyz".find(..)
    will not work because phrases:

        1. Use keys that contain space and other
           non alphanumeric characters.
        2. Uses boundaries that are non alphanumeric
           such as "Also so ..." contains only a single
           "so" because "Also" comes as a unit."
    """

    _punctuation = "[`~!@#$%^&*()_=+\[\]{}\\\|;:\"\'<>.,/?]"
    _whitespace = "[ \t\n\r]"

    # These chars mark boundaries that decide a phrase
    # can be separated in utext.
    PunctChars = _punctuation + _whitespace

    def __init__(self, aline, key):
        """
        aline = instance of Aline(). This contains
                addition info necessary for properly
                handling line wrap.
        key = Key in the phrase dictionary. This routine
                will attempt to find this in aline.
        """
        self.aline = aline

        self.utext = aline.text_line

        # 0 based column index into utext.
        self.col = 0

        # Used to stop testing on second line.
        # The 2nd line gets used for possible line wrap.
        self.first_line_len = self.aline.first_line_len

        # Normalize text string
        self.ltext = self.utext.lower()

        self.key = key

        # The previous char used in scanning
        self.prev_char = ''

    def __repr__(self):
        out = f"{self.__class__.__name__}("
        out += f"aline={self.aline},"
        out += f"col={self.col},"
        out += f"key='{self.key}',"
        out += f"prev_char='{self.prev_char}')"
        return out

    def is_char_punct(self, achar):
        """
        Test that achar is punctuation.
        """
        if achar in ExtractPhrase.PunctChars:
            return True
        return False

    def is_prev_char_punct(self):
        """
        Test that the previous char is punctuation.
        """
        if self.col == 0:
            return True
        # Assume prev_char contains a valid value
        if self.is_char_punct(self.prev_char):
            return True
        return False

    def is_eol_or_punct_at_end(self):
        """
        Test for end of line or punctuation.
        """
        if self.key == self.ltext:
            return True
        if len(self.key) > len(self.ltext):
            # Key is longer than ustring. No chance to extract phrase
            return False
        char_plus_one = self.ltext[len(self.key)]
        if self.is_char_punct(char_plus_one):
            return True
        return False

    def is_start_and_end(self):
        """
        Check the boundaries of start and end of
        the phrase.
        A valid start of phrasse is a punctuation.
        A valid end of phrase is punctuation or EOL.
        """
        if self.ltext.startswith(self.key) and \
            self.is_eol_or_punct_at_end():
            return True
        return False

    def is_col_in_2nd_line(self):
        """ Test is the col indicates it's in the 2nd line. """
        if self.col > self.aline.first_line_len:
            return True
        return False

    def is_proper_phrase(self):
        """
        Check boundaries of phrase for rules:
            * prev_char is_punct or start of line.
            * key matches string value
            * char at end of match is punct char
        """
        status = self.is_prev_char_punct() and \
                self.ltext.startswith(self.key) and \
                self.is_eol_or_punct_at_end()
        return status

    def extract_phrase(self):
        """
        Find the index of the phrase key in utext.

        If found, return column index of first instance.
        If not found, return -1
        """

        # Nothing found for empty params.
        if (self.utext == "") or (self.key == ""):
            return -1

        # Check for isolated key within ustring
        if self.is_start_and_end():
            return 0

        self.prev_char = self.ltext[0]
        self.col = 0
        # Skip to the start of a word, then test
        # Use prev_char as the trigger for testing
        while True:

            # Have finished the 1st line? 
            # If so, do not scan 2nd line
            if self.is_col_in_2nd_line():
                return -1

            if self.is_proper_phrase():
                return self.col

            if self.ltext == '':
                return -1

            # Prev char is not punctuation, try next col
            self.col += 1
            self.prev_char = self.ltext[0]
            self.ltext = self.ltext[1:]
            if self.ltext == "":
                return -1


class Flabby():
    """Initialize the dicts for handling input."""

    def __init__(self, user_args, parser):
        """Given the user args and the parser,
        load the flabby phrases into a dict
        and prepare for use.
        """
        self.user_args = user_args
        self.parser = parser
        self.phrase_dict = {}
        self.handle_output_file()
        self.load_phrase_dict() # Populates self.phrase_dict

        self.line_number = 0    # Line number in user text.
        self.text_lines = self.load_text_lines() # The user text
        if self.text_lines == []:
            # Nothing could be loaded.
            sys.exit(1)

        self.max_text_line_number = len(self.text_lines)

    def handle_output_file(self):
        """
        If user wants output redirected to a file,
        take use that file.
        If not requested, the output is stdout.
        """
        out_file = self.user_args["output"]
        try:
            if out_file:
                sys.stdout = open(out_file, 'w')
        except (OSError, IOError) as err:
            print(f"Cannot open --output {out_file}: {err}")
            self.parser.print_help()
            return 0

    def load_text_lines(self):
        """
        Load the entire user text document into a list.

        The existence of the input file has been determined
        by the mainline in argument checking.
        """
        # The above logic validates this duplicated code
        # and we know it will work.
        with open(self.user_args["input"]) as file_handle:
            raw_text_lines = file_handle.readlines()
        result = [aline.rstrip('\n') for aline in raw_text_lines]
        return result

    def load_phrase_dict(self):
        """Loads the phrases from a text file into a dict

        TODO key have options:
            'have a conversation (about)'
            'had a conversation (about)'
            'this is a (insert noun here) that'
            'i believe (that)'
            'i feel (that)'
        """

        # Use "hard coded" phrases at bottom of this file.
        lines = PHRASES

        for line_no, aline in enumerate(lines, start=1):
            # Grab the part up to ' - ' for the flabby phrase
            # Careful! The reason/better portion may have ' - '
            #
            # Sample text:
            # Accordingly – Use simpler replacement, such as so. Ex: Accordingly, be careful next time. Better: So, be careful next time.
            #     Advance warning – Redundant phrase. You don’t need advance. Ex: The storm hit with no advance warning. Better: The storm hit with no warning.
            if aline == '':
                break

            key, reason = re.match('(.*) - (.*)', str(aline)).groups()
            key = key.lower().strip()
            reason = reason.strip()
            comment_ndx = reason.find("Ex:", 0)
            comment_str = ""
            if comment_ndx == -1:
                print(f"line {line_no}: No Ex: for {reason}")
            else:
                comment_str = reason[0 : comment_ndx]

            phrase_entry = PhraseEntry(aline)
            self.phrase_dict[key] = [comment_str,
                                     phrase_entry.ex_better_list]
        return self.phrase_dict

    def get_next_user_text_input(self):
        """
        Obtain the next line of text to analyze.
        """

        try:
            return self.text_lines[self.line_number + 1]
        except:
            # print("Exceeding base line length")
            return " "

    def get_user_text_input(self):
        """
        Read a line and the next line from the input text.

        Return None when all lines have been returned.
        """
        if (self.line_number + 1) > len(self.text_lines):
            return None
        the_line = self.text_lines[self.line_number]
        first_line_len = len(the_line)
        # If the line is blank, ignore the next line
        if first_line_len != 0:
            next_line = self.get_next_user_text_input()
            flabby_line = ALine(self.line_number, the_line, next_line)
        else:
            flabby_line = ALine(self.line_number, the_line, "")

        self.line_number += 1
        return flabby_line

    def scan_for_flab(self, user_text_line):
        """
        Given ALine that contains line number and
        two lines of text input, scan for flab.
        Answers with a list of keys in phrase_dict
        of found flab, if any.

        Returns:
            A list of UserText that indicates flabs.
            No flab detected results in an empty list.
        """
        flabbys = []

        for flab_dict_entry in self.phrase_dict.keys():
            checker = ExtractPhrase(user_text_line, flab_dict_entry)
            flab_dict_ndx = checker.extract_phrase()
            if flab_dict_ndx != -1:
                flabbys.append(UserText(flab_dict_entry, user_text_line))
        return _flatten(flabbys)

    def flab_detector(self):
        """
        Detect flab on all the lines.

        Returns:
            List of UserText tuple containing dict key and oringinal line.
        """
        flabby_results_list = []
        while self.line_number <= len(self.text_lines):
            user_text_line = self.get_user_text_input()
            # End of user text input?
            if user_text_line is None:
                break
            scan_results = self.scan_for_flab(user_text_line)
            if scan_results:
                flabby_results_list.append(scan_results)
        results = _flatten(flabby_results_list)
        return results

    def output_flabby_list(self, flabs_list):
        """
        Given the list of detected flabby text, present to
        the user in a textual manner.
        """
        for flab in flabs_list:
            print(self.format_flabby_entry(flab))
        return True

    def format_flabby_entry(self, flab):
        """
        Form a string of one instance of flab.

        flab = {flab_dict_key, ALine(...)} # from the flabs_list

        Return this to the user suitable string for presentation.
        """
        # The remainder of the display get indented 4 spaces
        indent = " "*4

        try:
            dict_entry = self.phrase_dict[flab.key]
        except AttributeError as err:
            print(f"AttributeError: {err}")
            pdb.set_trace()

        # The +1 converts the 0 based lines #'s to human line #'s
        out = f"{flab.aline.line_number+1}: "
        out += f"{flab.aline.text_line}\n"
        out += indent + "==> " + flab.key + " <==\n"
        for ex_better in dict_entry[1]:
            ex, better = ex_better
            out += indent + "Example: " + ex + "\n"
            out += indent + "Better: " + better + "\n\n"
        return out


class PhraseEntry():
    """
    Parses the phrase text into flabby, reason and lists
    of Ex: and Betters.

    The flabby phrase entries look like:
        Advance warning - Redundant phrase. You don't need advance. Ex: The storm hit with no advance warning. Better: The storm hit with no warning.
        Are after - Clunky verb construction. Use follow, or seek, or desire, or want. Ex: The events are after the lecture. Better: The events follow the lecture. Ex: I don't know what you are after. Better: I don't know what you want.

    Notice the second one has TWO Ex: and Better: entries.
    """
    def __init__(self, aline):
        # Create two lines: the lead and a list of Ex:Better:
        #
        # Extract the Ex:...Better:... portion and chop it off
        # from aline.
        self.aline = aline
        ex_ndx = aline.find("Ex:")
        ex_better = aline[ex_ndx:] # line ending with Ex:...
        start_line = aline[:ex_ndx]     # line up to first Ex:
        key, reason = re.match(' *(.*)-(.*)', start_line).groups()
        self.key = key.lower().strip()
        self.reason = reason.strip()

        self.ex_better_list = self.breakup_ex_better(ex_better)

    def __repr__(self):
        out = f"aline={self.aline}: "
        out += f"\nkey={self.key}, "
        out += f"\nreason={self.reason}, "
        out += f"\nex_better_list={self.ex_better_list}"
        return out

    def breakup_ex_better(self, ex_better):
        """
        Break up the ex_better line into an array with
        each item containing an Ex:Better pain.

        Returns:
            List with Ex: Better only once per item.
        Example:
            ex_better = 'Ex: Testing only. Better: Short dict Ex: Testing Better: Still testing.'
            exb = self.breakup_ex_better(ex_better)
            # A list of tuples. Some flabbys have multiple Ex:Better:
            [("Testing only.", "Short dict"),
             ("Testing", "Still testing.")]
        """
        start = 0
        ex_better_list = []
        ex = "Ex:"
        better = "Better:"
        while True:
            ex_ndx = ex_better.find(ex, start)
            if ex_ndx == -1:
                break
            better_ndx = ex_better.find(better, start)
            if better_ndx == -1:
                print(f"{better} not found in {ex_better} ! ERROR")
                return ex_better_list

            ex_str = ex_better[ex_ndx + len(ex) : better_ndx]
            ex_str = ex_str.strip()

            start = better_ndx + len(better)
            #
            # Two cases for remainder of string:
            #    1. Another Ex: exists after Better: which is a
            #       positive value.
            #    2. Only a single Ex:Better: pair exists. Take
            #       the reminder of the string
            # Yank the ex portion
            # Yank the better portion
            ex_ndx = ex_better.find(ex, better_ndx + len(ex))
            if ex_ndx == -1:
                # No more Ex: in current line.
                better_end = len(ex_better)
            else:
                better_end = ex_ndx

            better_str = ex_better[better_ndx + len(better) : better_end]
            better_str = better_str.strip()
            ex_better_list.append((ex_str, better_str))

        return ex_better_list


class ALine():
    """
    What a single input line looks like internally.

    line_number = The line number in input text. Used
                  for messages.
    the_line    = A line in the input text.
    next_line   = Used because a flabby phrase may
                  et line wrapped.
                  At the end of file, next_line becomes "".

    This algorithm assumes "normal" writing styles with input
    text reasonably wide. Text with one or two words per line
    will break this utility as wrap-around only considers
    the next line.
    """
    def __init__(self, line_number, the_line, next_line):
        self.line_len = len(the_line)
        if len(next_line) > 0:
            self.text_line = the_line + " " + next_line
        else:
            self.text_line = the_line
        self.first_line_len = len(the_line)
        self.line_number = line_number
        # Keep the original line length to stop excessive
        # output of the next line for line wrap issues.
        self.orig_line_len = len(the_line)

    def __repr__(self):
        out = f'{self.__class__.__name__}('
        out += f'line_len={self.line_len!r},'
        out += f'first_line_len={self.first_line_len!r},'
        out += f'line_number={self.line_number},'
        out += f'text_line="{self.text_line}")'
        return out


def build_user_arg_dict(caller_argv):
    """Parse all the use command line arguments.

    Returns the param dictionay and the parser.
    The parset can be used to produce help
    messages in case of errors.
    """

    import textwrap
    parser = argparse.ArgumentParser(
            prog="Flabby writing utility",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog=textwrap.dedent('''\
                Usage Examples:
                ./flabby --help
                ./flabby --input MyBlog.md  # Output to terminal
                ./flabby -i great_blog.txt  # Output to terminal
                ./flabby --input my_blog.md --output my_blog.out
                ./flabby --input my_blog.md -o my_blog.out
            '''))

    parser.add_argument('-i', '--input',
            type=str,
            action='store',
            required=True,
            help='User document to scan for flabbiness.')

    parser.add_argument('-o', '--output',
            type=str,
            action='store',
            required=False,
            default=None,
            help='Output file of results.')


    args = parser.parse_args(args=caller_argv[1:])   # skip prog name

    # Convert the argparser params into a standard dict
    # The resulting dict contains all the params of
    # the command line.
    user_arg_dict = {}
    for key, value in vars(args).items():
        user_arg_dict[key] = value

    return (user_arg_dict, parser)


def does_input_file_exist(filename):
    # Determine input file existence.
    # Return True  if the input file exist.
    #        False if the input file does not exist.
    if not os.path.isfile(filename):
        print(f'Required input file does not exists: {filename}')
        return False
    return True

def is_output_file_writable(filename):
    # If an output file requested, the file could be written.
    # This validates that the output dir, if any, exists.
    # Return True  if the dir exists and the file could exist.
    #        False if the dir does not exist.
    if filename is None:
        # Output goes to stdout. This is OK.
        return True
    if filename:
        # The easiest way is to try to open this file.
        try:
            with open(filename, "w") as fh:
                pass
        except IOError as err:
            print(f"Cannot open output file: {err}")
            return False
    return True

def main(argv):
    """
    Run the flabby phrase utility.
    """
    (user_arg_dict, parser) = build_user_arg_dict(argv)

    status = does_input_file_exist(user_arg_dict["input"])
    if not status:
        return status

    status = is_output_file_writable(user_arg_dict["output"])
    if not status:
        return status

    flab = Flabby(user_arg_dict, parser)

    # With a flabby dictionary loaded, proceed to search
    # the user input for flabby phrases.
    flabs_list = flab.flab_detector()

    # If user wants output redirected to a file,
    # take use that file.
    out_file = user_arg_dict["output"]
    try:
        if out_file:
            sys.stdout = open(out_file, 'w')
    except (OSError, IOError) as err:
        print(f"Cannot open --output {out_file}: {err}")
        parser.print_help()
        return 1
    #except:
    #    print(f"Cannot open --output {out_file}: {err}")
    #    parser.print_help()
    #    return 0

    # Provide the user with a report of flabby phrases.
    flab.output_flabby_list(flabs_list)

    if user_arg_dict["output"]:
        sys.stdout.close()
    return 0

PHRASES = [
    'About - Try not to use this term when discussing quantities. Use “approximately" or a range instead. Ex: About 20 people attended. Better: Approximately 20 people attended. Or: Fifteen to twenty people attended.',
    "Absolutely essential - Redundant phrase. You don't need absolutely. Ex: Fresh eggs are absolutely essential to this recipe. Better: Fresh eggs are essential to this recipe.",
    "Absolutely necessary - Redundant phrase. You don't need absolutely. Ex: Reading is absolutely necessary to write well. Better: Reading is necessary to write well.",
    'Accordingly - Use simpler replacement, such as so. Ex: Accordingly, be careful next time. Better: So, be careful next time.',
    "Accuracy - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: The accuracy of his report wasn't good. Better: His report wasn't accurate.",
    "Actual facts - Redundant phrase. You don't need 'actual'. Ex: Listen to the actual facts of the case. Better: Listen to the facts of the case.",
    'Admit to - Flabby expression. Drop to. Ex: You should admit to stealing the coat. Better: You should admit stealing the coat.',
    "Advance forward - Redundant phrase. You don't need forward. Ex: The army advanced forward. Better: The army advanced.",
    "Advance planning - Redundant phrase. You don't need advance. Ex: The heist required advanced planning. Better: The heist required planning.",
    "Advance warning - Redundant phrase. You don't need advance. Ex: The storm hit with no advance warning. Better: The storm hit with no warning.",
    "Add an additional - Redundant phrase. You don't need an additional. Ex: Add an additional string to your bow. Better: Add a string to your bow.",
    "Add up - Redundant phrase. You don't need up. Ex: Add up your hours and see if you qualify for overtime. Better: Add your hours and see if you qualify for overtime.",
    "Added bonus - Redundant phrase. You don't need added. Ex: Winning the prize was an added bonus. Better: Winning the prize was a bonus.",
    'Almost - Use approximations such as this sparingly. Specific terms are better. Ex: It was almost time for class. Better: Class started in one minute.',
    'All of - Flabby expression. Drop of. Ex: All of the guests loved the party. Better: All the guests loved the party.',
    "All time record - Redundant phrase. You don't need all time. Ex: He broke the all time record for home runs. Better: He broke the record for home runs.",
    "All things being equal - Empty Phrase. Don't use it. Ex: All things being equal, we should arrive tonight. Better: If all goes well, we should arrive tonight.",
    "Alternative choice - Redundant phrase. You don't need choice. Ex: He had no alternative choice but to fight. Better: He had no alternative but to fight.",
    "All throughout - Redundant phrase. You don't need all. Ex: War exists all throughout history. Better: War exists throughout history.",
    'Analysis - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: Give an analysis of the data and then create a summary. Better: Analyze the data and then summarize it.',
    "And etc. - Redundant phrase. You don't need and. Ex: She loved dogs, cats, frogs, and etc. Better: She loved dogs, cats, frogs, etc.",
    "Anonymous stranger - Redundant phrase. You don't need anonymous. Ex: An anonymous stranger sent her flowers. Better: A stranger sent her flowers.",
    'Appearance - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: His appearance caused cheers from the crowd. Better: He appeared and the crowd cheered.',
    'Area - Vague Noun. Cut or use more specific word. Ex: James left the area. Better: James left Maryland.',
    "Are after - Clunky verb construction. Use follow, or seek, or desire, or want. Ex: The events are after the lecture. Better: The events follow the lecture. Ex: I don't know what you are after. Better: I don't know what you want.",
    "Armed gunman - Redundant phrase. You don't need armed. Ex: An armed gunman robbed the bank today. Better: A gunman robbed the bank today.",
    "As a matter of fact - Empty Phrase. Don't use it. Ex: As a matter of fact, I did eat all the candy. Better: Yes, I ate the candy.",
    "As being - Flabby expression. You don't need being. Ex: She is known as being the smartest in the school. Better: She is known as the smartest in the school.",
    "Ascend up - Redundant phrase. You don't need up. Ex: Ascend up the steps to reach the top. Better: Ascend the steps to reach the top.",
    "As far as I'm concerned - Empty Phrase. Don't use it. Ex: As far as I'm concerned, all politicians lie. Better: All politicians lie.",
    "Ask the question - Redundant phrase. You don't need the question. Ex: Ask the question to your mother. Better: Ask your mother.",
    "Aspect - Vague noun. Cut or use more specific word. Ex:  Commercials are an aspect of television I don't like. Better: I love television, but I hate commercials.",
    "Assemble together - Redundant phrase. You don't need together. Ex: Assemble together the parts included in the box. Better: Assemble the parts included in the box.",
    "As to whether - Flabby expression. You don't need as to. Ex: I didn't know as to whether he'd stay or go. Better: I didn't know whether he'd stay or go.",
    "As yet - Flabby expression. You don't need as. Ex: No word on survivors as yet. Better: No word on survivors yet.",
    "At all times - Empty phrase. Don't use, or fix. Ex: Be vigilant at all times. Better: Be vigilant.",
    'Attempt - Use simpler replacement, such as try. This word can be an example of nominalization too (verb or adjective turned into a noun). Use the verb or adjective form for more powerful sentences. Ex: Attempt it again. Better: Try again. Ex: His attempt at suicide was met with failure. Better: He attempted suicide but failed.',
    "At the end of the day - Empty Phrases. Don't use it. Ex: At the end of the day, the toughest survive. Better: The toughest survive.",
    "At the present time - Empty Phrase. Don't use or fix. Ex: I have no money at the present time. Better: I have no money now. I currently have no money.",
    "At this point in time - Empty Phrase. Don't use or fix. Ex: At this point in time, let's just forget about our plans. Better: Let's just forget about our plans.",
    "Bald-headed - Redundant phrase. You don't need headed. Ex: He was bald-headed. Better: He was bald.",
    "Basic necessities - Redundant phrase. You don't need basic. Ex: Prepare for disasters by stocking basic necessities. Better: Prepare for disasters by stocking necessities.",
    "Belief - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: It's his belief that editing can be done with ease. Better: He believes editing is easy.",
    'Best - How many other ways provide a superlative? Ex: Are you looking for the best socks? Better: Are you looking for the top-quality pair of socks? Ex: Are you looking for the best socks? Better: Are you looking for the pair of socks that are perfect for you?',
    'Big - Weak adjective. Replace with something more precise. Ex: He was a big man. Better: He was six feet tall and 250 pounds.',
    "Blend together - Redundant phrase. You don't need together. Ex: The colors blend together nicely. Better: The colors blend nicely.",
    "Bouquet of flowers - Redundant phrase. You don't need of flowers. Ex: The bouquet of flowers was beautiful. Better: The bouquet was beautiful.",
    "Brief moment - Redundant phrase. You don't need brief. Ex: For a brief moment, he was speechless. Better: For a moment, he was speechless.",
    'Brilliance - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: Not all posts achieve brilliance. Better: Not all posts are brilliant.',
    "Cameo appearance - Redundant phrase. You don't need appearance. Ex: The actor's cameo appearance caused a riot. Better: The actor's cameo caused a riot.",
    'Care about - Flabby verb construction. Use value or like to save a word. Ex: Do your readers care about grammar? Better: Do your readers value grammar?',
    "Careful scrutiny - Redundant phrase. You don't need careful. Ex: The lawyer read the document with careful scrutiny. Better: The lawyer read the document with scrutiny. Best: The lawyer scrutinized the document.",
    'Carelessness - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: Her carelessness caused his death. Better: He died because she was careless.',
    'Catch on - Flabby verb construction. Use resonate or spread. Ex: Hopefully the message will catch on. Better: Hopefully the message will spread.',
    'Caused a drop in X - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: Pay cuts caused a drop in morale within our company. Better: Pay cuts demoralized our company.',
    'Caused considerable confusion - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. In this case, use something more powerful, such as confused or baffled. Ex: The instructions caused considerable confusion in the class. Better: The instructions baffled the class.',
    "Cease and desist - Redundant phrase. You don't need and desist. Ex: Cease and desist all contact with Mrs. Jones. Better: Cease all contact with Mrs. Jones.",
    "Close proximity - Redundant phrase. You don't need close. Ex: The close proximity of the tourists caused the elephant to charge. Better: The proximity of the tourists caused the elephant to charge.",
    "Closed fist - Redundant phrase. You don't need closed. Ex: He hit me with his closed fist. Better: He hit me with his fist.",
    "Commute back and forth - Redundant phrase. You don't need back and forth. Ex: His commute back and forth exhausted him. Better: His commute exhausted him.",
    'Comparison - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: He made a comparison between apples and oranges. Better: He compared apples with oranges.',
    "Completely destroy - Redundant phrase. You don't need completely. Ex: Joe completely destroyed his room. Better: Joe destroyed his room.",
    "Completely eliminate - Redundant phrase. You don't need completely. Ex: You must completely eliminate your foes. Better: You must eliminate your foes.",
    "Completely engulfed - Redundant phrase. You don't need completely. Ex: Flames completely engulfed the house. Better: Flames engulfed the house.",
    "Completely filled - Redundant phrase. You don't need completely. Ex: He completely filled his cup. Better: He filled his cup.",
    "Connect together - Redundant phrase. You don't need together. Ex: Connect together the two wires. Better: Connect the two wires.",
    "Could possibly - Redundant phrase. You don't need possibly. Ex: You could possibly win. Better: You could win.",
    "Crisis situation - Redundant phrase. You don't need situation. Ex: In a crisis situation try to relax and think clearly. Better: In a crisis try to relax and think clearly.",
    "Current trend - Redundant phrase. You don't need current. Ex: Some say blogging is a current trend that won't last. Better: Some say blogging is a trend that won't last.",
    'Cut down on - Flabby Phrasal Verb. Use reduce or limit. Ex: You should cut down on your sugar intake. Better: You should limit your sugar intake.',
    'Decrease in strength - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: The Euro decreased in strength against the US Dollar. Better: The Euro weakened against the US Dollar.',
    'Definition - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: His definition of fun was sleeping and watching television. Better: He defined fun as sleeping and watching television.',
    "Depreciate in value - Redundant phrase. You don't need in value. Ex: Assets depreciate in value as each year passes. Better: Assets depreciate as each year passes.",
    "Descend down - Redundant phrase. You don't need down. Ex: Descend down the steps to exit the building. Better: Descend the steps to exit the building.",
    'Description - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: Please give a description of the man who attacked you. Better: Please describe the man who attacked you.',
    "Desirable benefit - Redundant phrase. You don't need desirable. Ex: What desirable benefit does writing offer? Better: What benefit does writing offer?",
    "Did not have much confidence in - Avoid using negative constructions if possible. Readers don't like when you tell them what something is not. They like when you tell them what something is. Use distrusted or doubted. Ex: The soldiers did not have much confidence in their officers. Better: The soldiers doubted their officers' abilities.",
    "Did not pay attention to - Avoid using negative constructions if possible. Readers don't like when you tell them what something is not. They like when you tell them what something is. Use ignored. Ex: The soldiers did not listen to their officers. Better: The soldiers ignored their officers' orders.",
    "Did not remember - Avoid using negative constructions if possible. Readers don't like when you tell them what something is not. They like when you tell them what something is. Use forgot. Ex: The soldiers did not remember their orders. Better: The soldiers forgot their orders.",
    "Different kinds - Redundant phrase. You don't need different. Ex: The chart lists five different kinds of animals. Better: The chart lists five kinds of animals.",
    "Difficulty - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: I'm having difficulty with math. Better: Math is difficult for me. Best: I'm struggling with math.",
    'Due to - Clunky expression. Use because or revise. Ex: He got wet due to the rain. Better: He got wet because it rained. Best: The rain got him wet.',
    'Due to the fact that - Empty phrase. Delete or use because or since. Ex: Due to the fact that I write, I love books. Better: Because I write, I love books.',
    "During the course of - Redundant phrase. You don't need the course of. Ex: The forecast will change during the course of the day. Better: The forecast will change during the day.",
    "Dwindle down - Redundant phrase. You don't need down. Ex: She loved to shop, so her savings dwindled down. Better: She loved to shop, so her savings dwindled.",
    "Each and every - Redundant phrase. You don't need and every. Ex: I loved each and every one of them. Better: I loved each one of them.",
    'Ease - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: He thinks editing is a task you can do with ease. Better: He thinks editing is easy.',
    "Eliminate altogether - Redundant phrase. You don't need altogether. Ex: We should reduce or eliminate altogether speeding ticket fines. Better: We should reduce or eliminate speeding ticket fines.",
    "Eliminate entirely - Redundant phrase. You don't need entirely. Ex: We could eliminate entirely testing and students would still learn. Better: We could eliminate testing and students would still learn.",
    "Emergency situation - Redundant phrase. You don't need situation. Ex: We have an emergency situation at the school. Better: We have an emergency at the school.",
    "Empty out - Redundant phrase. You don't need out. Ex: Empty out the dishwasher. Better: Empty the dishwasher.",
    "End result - Redundant phrase. You don't need end. Ex: Study and the end results will please you. Better: Study and the results will please you.",
    'Encouragement - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: His encouragement helped my success. Better: He encouraged me and I succeeded.',
    "Enter in - Redundant phrase. You don't need in. Ex: Enter in your name and email address. Better: Enter your name and email address.",
    "Equal to one another - Redundant phrase. You don't need to one another. Ex: They are equal to one another in size, but Joe is faster. Better: They are equal in size, but Joe is faster.",
    "Eradicate completely - Redundant phrase. You don't need completely. Ex: We must eradicate completely these roaches. Better: We must eradicate these roaches.",
    "Every single person - Redundant phrase. You don't need single (unless referring to marital status). Ex: Every single person should attend. Better: Every person should attend. Or: Everyone should attend.",
    "Evolve over time - Redundant phrase. You don't need over time. Ex: Relationships evolve over time. Better: Relationships evolve.",
    "Exact same - Redundant phrase. You don't need exact. Ex: They spoke at the exact same time. Better: They spoke at the same time.",
    'Facilitate - Use simpler replacement, such as help, yield, or aid. Ex: Patience facilitates understanding. Better: Patience aids understanding.',
    'Facility - Stilted phrase. Say exactly what an object is (school, hospital, government building). Ex: The facility had a large cafeteria. Better: Johnson Elementary School had a large cafeteria.',
    'Factor - Dull, unnecessary word. Replace with a verb. Ex: Avid reading was a factor in his writing ability. Better: Avid reading helped his writing.',
    "Failure - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: His failure was caused by not studying hard enough. Better: He failed because he didn't study hard enough.",
    "Fall down - Redundant phrase. You don't need down. Ex: If you fall down, try again. Better: If you fall, try again.",
    "Felldown - Redundant phrase. You don't need down. Ex: If you fall down, try again. Better: If you fall, try again.",
    "Favorable approval - Redundant phrase. You don't need favorable. Ex: The drawings received favorable approval from the planning board. Better: The drawings received approval from the planning board. Best: The planning board approved the drawings.",
    "Fellow classmate - Redundant phrase. You don't need fellow. Ex: A fellow classmate teased Johnny. Better: A classmate teased Johnny.",
    "Fellow colleague - Redundant phrase. You don't need fellow. Ex: A fellow colleague saw Jim stealing the office supplies. Better: A colleague saw Jim stealing the office supplies.",
    "Few in number - Redundant phrase. You don't need in number. Ex: First-time homebuyers are too few in number to absorb excess inventory. Better: First-time homebuyers are too few to absorb excess inventory.",
    "Figure out - Clunky verb construction. Use determine, guess, or decide. Ex: I can't figure out who's who. Better: I can't determine who's who.",
    "Filled to capacity - Redundant phrase. You don't need to capacity. Ex: The stadium was filled to capacity with anxious fans. Better: The stadium was filled with anxious fans. Best: Anxious fans filled the stadium.",
    "Final conclusion - Redundant phrase. You don't need final. Ex: He came to a final conclusion that he hated his job. Better: He came to a conclusion that he hated his job. Best: He concluded that he hated his job.",
    'Finally - Weak linking term. Be more precise. Ex: Finally, he got the job. Better: After five interviews, he got the job.',
    "Final outcome - Redundant phrase. You don't need final. Ex: Death was the final outcome. Better: Death was the outcome.",
    "Final ultimatum - Redundant phrase. You don't need final. Ex: I gave him a final ultimatum. Better: I gave him an ultimatum.",
    "Find out - Clunky verb construction. Use determine, or learn. Ex: Find out what matters and what doesn't. Better: Learn what matters and what doesn't.",
    "First and foremost - Redundant phrase. You don't need first and. Ex: He remains first and foremost a businessman. Better: He remains foremost a businessman.",
    "First conceived - Redundant phrase. You don't need first. Ex: He first conceived the idea to start a business while he was a freshman in college. Better: He conceived the idea to start a business while he was a freshman in college.",
    "First of all - Redundant phrase. You don't need of all. Ex: First of all, I didn't tell him your name. Better: First, I didn't tell him your name.",
    "Fly through the air - Redundant phrase. You don't need through the air. Ex: The bird flew through the air above us. Better: The bird flew above us.",
    "Flew through the air - Redundant phrase. You don't need through the air. Ex: The bird flew through the air above us. Better: The bird flew above us.",
    "For all intents and purposes - Empty phrase. Don't use it. Ex: For all intents and purposes, the relationship was doomed. Better: The relationship was doomed.",
    "Foreign imports - Redundant phrase. You don't need foreign. Ex: He believes foreign imports hurt our country's economy. Better: He believes imports hurt our country's economy.",
    "Former graduate - Redundant phrase. You don't need former. Ex: She was a former graduate of Harvard. Better: She was a graduate of Harvard. Best: She was a Harvard graduate.",
    "For the most part - Empty phrase. Don't use it. Ex: For the most part, I enjoy editing. Better: I enjoy editing.",
    "For the purpose of - Empty phrase. Don't use. Ex: I practice yoga for the purpose of improving my posture. Better: I practice yoga to improve my posture.",
    "Former veteran - Redundant phrase. You don't need former. Ex: Uncle Bob was a former veteran of Vietnam. Better: Uncle Bob was a veteran of Vietnam. Best: Uncle Bob was a Vietnam veteran.",
    "Free gift - Redundant phrase. You don't need free. Ex: You get a free gift if you complete the survey. Better: You get a gift if you complete the survey.",
    'Frequently - Imprecise Phrase. Use something more specific. Ex: I frequently wash my car. Better: I wash my car daily.',
    "Frozen ice - Redundant phrase. You don't need frozen. Ex: He fell through the frozen ice. Better: He fell through the ice.",
    "Frozen tundra - Redundant phrase. You don't need frozen. Ex: The frozen tundra was stretched out before them. Better: The tundra was stretched out before them.",
    "Fuse together - Redundant phrase. You don't need of together. Ex: Fuse together the wires and continue with the next step. Better: Fuse the wires and continue with the next step.",
    "Future plans - Redundant phrase. You don't need future. Ex: What are your future plans for college? Better: What are your plans for college?",
    "Gather together - Redundant phrase. You don't need together. Ex: Gather together your things and leave. Better: Gather your things and leave.",
    "General public - Redundant phrase. You don't need general. Ex: The portable bathrooms are for the general public. Better: The portable bathrooms are for the public.",
    'Get - Weak verb. Cut it or use stronger verbs such as become, land, acquire, or retrieve. Ex: You need to get motivated. Better: Motivate yourself. Ex: How many clients did you get through blogging? Better: How many clients did you land through blogging?',
    'Get out of - Weak phrasal verb. Use exit. Ex: Get out of the building. Better: Exit the building.',
    "Give in - Weak phrasal verb. Use concede, or quit. Ex: Don't give in. Better: Don't quit.",
    "Go ahead and - Clunky expression. You don't need it. Just start with the verb that follows this expression. Ex: I might have to go ahead and call the cops. Better: I might have to call the cops.",
    "Go back over - Clunky verb construction. Use reread, reexamine, or reevaluate. Ex: Let's go back over the case files. Better: Let's reexamine the case files.",
    'Go into - Clunky verb construction. Use enter; or visit, discuss, or explain. Ex: I will go into the school today. Better: I will visit the school today. Ex: I will go into detail about blogging during the lecture. Better: I will explain blogging during the lecture.',
    "Go on - Flabby verb construction. Use continue. Ex: I could go on quoting famous people, but I won't. Better: I could continue quoting famous people, but I won't.",
    'Good - Just not good enough. When qualifying something as "good", think about how good it is. Refer to something slightly better, more suitable, or something that\'s really good. Ex: Just not good enough. Better: More excellent choices exist.',
    "Grateful every day - Flabby phrase. Use eternally grateful. Ex: I'm grateful every day. Better: I'm eternally grateful.",
    'Great - Stronger than "good", but not my much. Ex: A great idea. Better: A strong idea. A fantastic opportunity. A wonderful idea. Ex: I felt great. Better: I felt fantastic all over!',
    "Grew in size - Redundant phrase. You don't need in size. Ex: He grew in size since I last saw him. Better: He grew since I last saw him.",
    "Grow in size - Redundant phrase. You don't need in size. Ex: He grew in size since I last saw him. Better: He grew since I last saw him.",
    "Grown in size - Redundant phrase. You don't need in size. Ex: He grew in size since I last saw him. Better: He grew since I last saw him.",
    'Had a discussion concerning - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: We had a discussion concerning the proposed changes. Better: We discussed the proposed changes.',
    'Had a conversation (about) - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: We had a conversation about money. Better: We talked money.',
    'Have a conversation (about) - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: We had a conversation about money. Better: We talked money.',
    "Harder than it has to be - Empty phrase. Use harder than necessary. Ex: You're making it harder than it has to be. Better: You're making it harder than necessary.",
    'Has to be - Clunky verb construction. Use must be. Ex: This has to be the right place. Better: This must be the right place. Ex: I have to be strong for her. Better: I must be strong for her.',
    'Have got - Avoid "got" when possible. Just use "have". In almost any situation "got" does not add to your writing. Ex: She got to her feet. Better: She jumped to her feet.',
    'Have to be - Clunky verb construction. Use must be. Ex: This has to be the right place. Better: This must be the right place. Ex: I have to be strong for her. Better: I must be strong for her.',
    'Have a need for - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: Do you have a need for me? Better: Do you need me?',
    "Heat up - Redundant phrase. You don't need up. Ex: Heat up the soup. Better: Heat the soup.",
    'Helps keep - Clunky verb construction. Use keeps or another strong verb. Ex: Outlining helps keep your thoughts straight. Better: Outlining clarifies your thoughts.',
    "Here's the thing - Colloquial expression. You can do without it. Ex: Here's the thing. Better: Just eliminate that phrase.",
    "Hollow tube - Redundant phrase. You don't need hollow. Ex: He slid down the hollow tube at the water park. Better: He slid down the tube at the water park.",
    'I feel - Timid expression. If you believe something, just say it. Besides, you can\'t “feel" an opinion. Ex: I feel that college isn\'t that much fun. Better: College sucks!',
    'I feel that - Timid expression. If you believe something, just say it. Besides, you can\'t “feel" an opinion. Ex: I feel that college isn\'t that much fun. Better: College sucks!',
    'I believe - Timid expression. If you believe something, just say it. Ex: I believe everyone should study music. Better: Everyone should study music.',
    "If you need to - Flabby if clause. Rework the sentence. Ex: If you need to get more clients, you need to market yourself properly. Better: Market yourself properly and you'll gain more clients.",
    "If you wish like to - Flabby if clause. Rework the sentence. Ex: If you want to get good grades, listen to your teachers. Better: Listen to your teachers and you'll get good grades.",
    "If you want like to - Flabby if clause. Rework the sentence. Ex: If you want to get good grades, listen to your teachers. Better: Listen to your teachers and you'll get good grades.",
    "If you would like to - Flabby if clause. Rework the sentence. Ex: If you want to get good grades, listen to your teachers. Better: Listen to your teachers and you'll get good grades.",
    'Important - Readers eyes will skip this because "important" gets seen too often. Distinguish important information by providing a colorful description - but don\'t go overboard. Ex: Don\'t skip the important steps. Better: Don\'t skip the critical steps. Ex: Know the times for important action. Better: Know the times for critical action.',
    "I might add - Flabby phrase. Delete it. Ex: I'm an excellent writer, I might add. Better: I'm an excellent writer.",
    "Increase in strength - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: You'll see an increase in strength with exercise. Better: Exercise will strengthen your body.",
    "Individual - Whenever possible and appropriate, use a simpler replacement, such as man, woman, or person. Ex: If you're the type of individual who likes adventure, skydiving is for you. Better: If you're an adventurous person, skydiving is for you.",
    'Initial - Whenever possible and appropriate, use a simpler replacement, such as first. Ex: My initial thought was to flee. Better: My first thought was to flee."',
    "Integrate with each other - Redundant phrase. You don't need with each other. Ex: The two systems must integrate with each other to share data. Better: The two systems must integrate to share data.",
    'Intensity - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: She has a high level of intensity. Better: She is intense.',
    'Intention is - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: My intention is to sleep all day. Better: I intend to sleep all day.',
    "In terms of - Flabby phrase. Delete it. Ex: The job offer was tempting in terms of salary. Better: The job's salary was tempting.",
    'In my opinion - Flabby phrase. Delete it. Ex: In my opinion, blogging rocks! Better: Blogging rocks!',
    "In order to - Redundant phrase. You don't need in order. Ex: In order to succeed, you must work hard. Better: To succeed, you must work hard.",
    "In spite of that fact that - Flabby phrase. Use although. Ex: In spite of that fact that I'm rich, I don't own a car. Better: Although I'm rich, I don't own a car.",
    "In the event of - Flabby phrase. Use if. Ex: In the event of someone pointing a gun at you, don't resist. Better: If someone points a gun at you, don't resist.",
    "In the event that - Flabby phrase. Use if. Ex: In the event that you win, you'll receive a trophy. Better: If you win, you'll receive a trophy.",
    "In the process of - Flabby phrase. Delete it. Ex: I'm in the process of quitting my job. Better: I'm quitting my job.",
    "Introduced a new - Redundant phrase. You don't need a new. Ex: They introduced a new software upgrade. Better: They introduced a software upgrade.",
    "Introduced for the first time - Redundant phrase. You don't need for the first time. Ex: The new owners were introduced for the first time at the company meeting. Better: The new owners were introduced at the company meeting.",
    'Investigation - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: My investigation led to solving the case. Better: I investigated and solved the case.',
    "Is after - Clunky verb construction. Use follow, or seek, or desire, or want. Ex: The events are after the lecture. Better: The events follow the lecture. Ex: I don't know what you are after. Better: I don't know what you want.",
    'Is aware of - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: He is aware of his bad reputation. Better: He knows his reputation stinks.',
    'Is in love with - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: He is in love with Judy. Better: He loves Judy.',
    'Is interesting to me - Weak "to be" verb construction. Revise it. Ex: Editing is interesting to me. Better: Editing interests me.',
    "It is - Grammar expletive that robs your sentence of strength. Avoid it. Ex: It's two hours before the game starts. Better: The game starts in two hours.",
    "It's - Grammar expletive that robs your sentence of strength. Avoid it. Ex: It's two hours before the game starts. Better: The game starts in two hours.",
    'It seems like - Flabby phrase & a grammar expletive. Delete it. Ex: It seems like you hate me. Better: Apparently you hate me.',
    'It would be - Grammar expletive that robs your sentence of strength. Avoid it. Ex: It would be polite if you said hi to her. Better: Be polite and say hi to her. Ex: It would be nice if we had more vacation time. Better: I wish we had more vacation time.',
    "Join together - Redundant phrase. You don't need together. Ex: They join together as one. Better: They join as one.",
    "Joint collaboration - Redundant phrase. You don't need joint. Ex: The joint collaboration between state and federal agencies failed. Better: The collaboration between state and federal agencies failed.",
    'Just - A word similar to "really" and, for the most part, should be deleted. Ex: Just do it! Better: Do it! Ex: You could do just about anything to help. Better: You could do almost anything to help.',
    "Kneel down - Redundant phrase. You don't need down. Ex: Kneel down before Zod. Better: Kneel before Zod.",
    "Knowledgeable expert - Redundant phrase. You don't need knowledgeable. Ex: She's a knowledgeable expert in her field. Better: She's an expert in her field.",
    "Lacked the ability to - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences, such as wasn't able to or couldn't. Ex: He lacked the ability to read. Better: He couldn't read.",
    "Later time - Redundant phrase. You don't need time. Ex: Call me at a later time. Better: Call me later.",
    'Led to the destruction of - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: The fire led to the destruction of the town. Better: The fire destroyed the town.',
    "Lift up - Redundant phrase. You don't need up. Ex: Lift up the weight. Better: Lift the weight.",
    "Live studio audience - Redundant phrase. You don't need live. Ex: The band played to a live studio audience. Better: The band played to a studio audience.",
    'Literally - Is it actually literal? This word can get old fast. "Literally" means metaphorically. It prevents people from thinking up a fresh metaphor for their description. Ex: I spent literally every cent on that gift. Better: I spent all my savings on that gift.',
    'Made a decision to - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: He made a decision to leave. Better: He decided to leave.',
    'Made an announcement - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: He made an announcement that he was getting married. Better: He announced he was getting married.',
    'Make an announcement - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: He made an announcement that he was getting married. Better: He announced he was getting married.',
    'Made it to - Flabby phrase & a grammar expletive. Use arrived, or reached. Ex: They made it to their destination. Better: They reached their destination.',
    "Made out of - Redundant phrase. You don't need out. Ex: It was made out of wood. Better: It was made of wood.",
    "Major breakthrough - Redundant phrase. You don't need major. Ex: The invention was a major breakthrough in nuclear technology. Better: The invention was a breakthrough in nuclear technology.",
    "Major feat - Redundant phrase. You don't need major. Ex: Bending horseshoes is a major feat of strength few can match. Better: Bending horseshoes is a feat of strength few can match.",
    'Many - Often vague. Fine for an indeterminate group of things. If you have an idea of the actual number, use that. Ex: Many people attended. Better: About twenty people attended. Ex: They had many ideas. Better: They had a multitude of ideas.',
    "May possibly - Redundant phrase. You don't need possibly. Ex: She may possibly get the job. Better: She may get the job.",
    "Might possibly - Redundant phrase. You don't need possibly. Ex: She may possibly get the job. Better: She may get the job.",
    'Meaningful - Weak adjective. Delete it or redo your sentence. Ex: It was a meaningful gesture. Better: The gesture touched me.',
    "Meet together - Redundant phrase. You don't need together. Ex: The two roads meet together at the traffic circle. Better: The two roads meet at the traffic circle.",
    "Meet with each other - Redundant phrase. You don't need with each other. Ex: We met with each other to discuss her offer. Better: We met to discuss her offer.",
    "Merge together - Redundant phrase. You don't need together. Ex: Our companies should merge together. Better: Our companies should merge.",
    "Mix together - Redundant phrase. You don't need together. Ex: Oil and water don't mix together. Better: Oil and water don't mix.",
    "Most unique - Redundant phrase. You don't need most. Ex: His poetry is most unique. Better: His poetry is unique.",
    'Movement - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: My movement startled the cat. Better: I moved and startled the cat.',
    "Mutual cooperation - Redundant phrase. You don't need mutual. Ex: We need mutual cooperation to succeed. Better: We need cooperation to succeed.",
    "Mutual respect for each other - Redundant phrase. You don't need for each other. Ex: My father and I have mutual respect for each other. Better: My father and I have mutual respect. Or: My father and I respect each other.",
    "Need to do to - Clunky verb construction. Use need to or must do. Ex: That's all you need to do to succeed. Better: That's all you must do to succeed.",
    "Never before - Redundant phrase. You don't need before. Ex: Never before have I been so offended. Better: Never have I been so offended.",
    "New innovation - Redundant phrase. You don't need new. Ex: It was a new innovation to content marketing. Better: It was an innovation to content marketing.",
    "New invention - Redundant phrase. You don't need new. Ex: The new invention would change the world. Better: The invention would change the world.",
    "None at all - Redundant phrase. You don't need at all. Ex: None at all survived. Better: None survived.",
    "Not honest - Avoid using negative constructions if possible. Try to say what something is instead. Ex: He is not honest. Better: He's dishonest.",
    "Not important - Avoid using negative constructions if possible. Try to say what something is instead. Ex: It's not important. Better: It's unimportant/trivial/minor.",
    'Note that - Usually redundant phrase. Just state the rest of the sentence. Ex: Note that this product sells itself. Better: This product sells itself.',
    'Notice that - Usually redundant phrase. Just state the rest of the sentence. Ex: Notice that this product sells itself. Better: This product sells itself.',
    "Now pending - Redundant phrase. You don't need now. Ex: Our request is now pending. Better: Our request is pending.",
    "Off of - Redundant phrase. You don't need of. Ex: Get your plate off of the counter. Better: Get your plate off the counter.",
    'Offered a suggestion - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: I offered a suggestion of a place to eat. Better: I suggested a place to eat.',
    'On a regular basis - Flabby phrase. Use regularly instead. Ex: I exercise on a regular basis. Better: I exercise regularly.',
    "Open up - Redundant phrase. You don't need up (unless talking about someone revealing something to you). Ex: Open up the windows. Better: Open the windows.",
    "Originally created - Redundant phrase. You don't need originally. Ex: Nobody truly knows when the world was originally created. Better: Nobody truly knows when the world was created.",
    "Other - Used to apply to basically any situation where you're trying to figure something out with a person. Ex: Any other suggestions? Better: Any alternative suggestions? Any further suggestions? Any different suggestions?",
    "Outside in the yard - Redundant phrase. You don't need outside. Ex: The kids are playing outside in the yard. Better: The kids are playing in the yard.",
    "Outside of - Redundant phrase. You don't need 'of'. Ex: He puked outside of the bar. Better: He puked outside the bar.",
    "Over exaggerate - Redundant phrase. You don't need over. Ex: She tends to over exaggerate. Better: She tends to exaggerate.",
    "Palm of my hand - Redundant phrase. You don't need 'of my hand'. Ex: He placed the gun in the palm of my hand. Better: He placed the gun in my palm.",
    "Passing fad - Redundant phrase. You don't need 'passing'. Ex: Selfies are a passing fad. Better: Selfies are a fad.",
    "Past experience - Redundant phrase. You don't need 'past'. Ex: My past experiences are what made me who I am today. Better: My experiences are what made me who I am today.",
    "Penetrate into - Redundant phrase. You don't need 'into'. Ex: The bullet can easily penetrate into the wood. Better: The bullet can easily penetrate the wood.",
    "Period of time - Redundant phrase. You don't need 'of time'. Ex: Dinosaurs ruled during that period of time. Better: Dinosaurs ruled during that period.",
    "Personal friend - Redundant phrase. You don't need 'personal'. Ex: He's a personal friend of mine. Better: He's a friend of mine.",
    "Personal opinion - Redundant phrase. You don't need 'personal'. Ex: It's just my personal opinion. Better: It's just my opinion.",
    "Pick and choose - Redundant phrase. You don't need 'and choose'. Ex: Pick and choose your friends wisely. Better: Pick your friends wisely.",
    'Pick out - Flabby phrase. Use choose instead. Ex: Pick out an outfit to wear. Better: Choose an outfit to wear.',
    "Pick up on - Flabby phrase. Use notice, or sense instead. Ex: He didn't pick up on the subtle nuances. Better: He didn't notice the subtle nuances.",
    'Play up - Flabby phrase. Use emphasize instead. Ex: You need to play up your best features. Better: You need to emphasize your best features.',
    "Plunge down - Redundant phrase. You don't need down. Ex: The stock market plunged down today. Better: The stock market plunged today.",
    'Point out - Flabby phrase. Use emphasize, say, mention, or state instead. Ex: Let me point out the rules first. Better: Let me mention the rules first.',
    "Polar opposites - Redundant phrase. You don't need polar. Ex: The two friends are polar opposites. Better: The two friends are opposites.",
    "Postpone until later - Redundant phrase. You don't need until later. Ex: You should postpone your appointment until later. Better: You should postpone your appointment.",
    "Pouring down rain - Redundant phrase. You don't need down. Ex: The pouring down rain ruined the picnic. Better: The pouring rain ruined the picnic.",
    "Preheat - Redundant phrase. You don't need pre. Ex: Preheat the oven before you prepare your ingredients. Better: Heat the oven before you prepare your ingredients.",
    "Present time - Redundant phrase. You don't need time. Ex: He's not available at the present time. Better: He's not available at present.",
    "Protest against - Redundant phrase. You don't need against. Ex: You must protest against tyranny. Better: You must protest tyranny.",
    'Put off - Flabby phrase. Use postpone, delay, or stall instead. Ex: He put off his dentist appointment. Better: He postponed his dentist appointment.',
    "Put together - Flabby phrase. Use assemble, build, or built instead. Ex: They put together the child's toy. Better: They assembled the child's toy.",
    "Raise up - Redundant phrase. You don't need up Ex: Raise up the flag. Better: Raise the flag.",
    'Reaction - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: My reaction caused everyone to be surprised. Better: The way I reacted surprised everyone.',
    "Really - Flabby modifier. Try to do without, or think of a more powerful word you are modifying. Ex: I'm really hungry. Better: I'm starving.",
    "Reason why - Redundant phrase. You don't need why. Ex: I'll never know the reason why she left. Better: I'll never know the reason she left. Or: I'll never know why she left.",
    "Refer back - Redundant phrase. You don't need back. Ex: You'll have to refer back to the instructions. Better: You'll have to refer to the instructions.",
    'Refusal - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: His refusal to leave forced me to call the cops. Better: He refused to leave, so I called the cops.',
    "Reply back - Redundant phrase. You don't need back. Ex: Reply back to this email to get the special offer. Better: Reply to this email to get the special offer.",
    'Resulted in a decrease - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: The mandate resulted in an increase in taxes. Better: The mandate increased taxes.',
    'Resulted in a increase - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: The mandate resulted in an increase in taxes. Better: The mandate increased taxes.',
    "Revert back - Redundant phrase. You don't need back. Ex: Revert back to the saved file if you experience problems. Better: Revert to the saved file if you experience problems.",
    "Safehaven - Redundant phrase. You don't need safe. Ex: That area is a safe haven for smugglers. Better: That area is a haven for smugglers.",
    "Same exact - Redundant phrase. You don't need exact. Ex: I have the same exact phone cover as you. Better: I have the same phone cover as you.",
    "Serious danger - Redundant phrase. You don't need serious. Ex: You're in serious danger. Better: You're in danger.",
    'Shock - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: What he revealed caused a shock to his family. Better: What he revealed shocked his family.',
    'Short - Weak Adjective. Replace with something more precise. Ex: Bob was a short man. Better: Bob was four feet tall.',
    'Show up - Weak verb construction. Use appear, enter, visit, or arrive instead. Ex: If you show up early, call me. Better: If you arrive early, call me.',
    'Situation - Vague noun. Be more specific if possible. Ex: The situation got worse. Better: The riot got worse.',
    'Small - Weak Adjective. Replace with something more precise. Ex: My desk is small. Better: My desk is only three feet wide.',
    'So - Unnecessary intensifier. Delete. Ex: It was so delightful. Better: It was delightful.',
    "Spell it out in detail - Redundant phrase. You don't need in detail.And you can use define, or explain instead. Ex: Did you spell it out in detail for him? Better: Did you spell it out for him? Or: Did you explain it to him?",
    'Spend - If this word is followed by an ing verb, modify your sentence. Ex: How many hours do you spend writing each day? Better: How many hours do you write each day?',
    "Start off - Redundant phrase. You don't need off/out. Ex: Let me start off by saying thanks. Better: Let me start by saying thanks.",
    "Start out - Redundant phrase. You don't need off/out. Ex: Let me start off by saying thanks. Better: Let me start by saying thanks.",
    "Starts to - Redundant phrase. You don't need to. Ex: If it starts to rain, close the window. Better: If it starts raining, close the window.",
    "Still persist - Redundant phrase. You don't need still. Ex: If symptoms still persist, call your doctor. Better: If symptoms persist, call your doctor.",
    "Still remains - Redundant phrase. You don't need still. Ex: Even after all the bombing raids, the building still remains. Better: Even after all the bombing raids, the building remains.",
    'Stuff - Meaningless word. Create a more explicit phrase. Ex: Get the stuff before we go. Better: We need the camping gear loaded.',
    "Sudden impulse - Redundant phrase. You don't need sudden. Ex: I had a sudden impulse for chocolate cake. Better: I had an impulse for chocolate cake.",
    'Surprise - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Use the verb or adjective form for more powerful sentences. Ex: Her actions were the cause of his surprise. Better: Her actions surprised him.',
    "Surrounded on all sides - Redundant phrase. You don't need on all sides. Ex: They were surrounded on all sides by enemies. Better: They were surrounded by enemies. Or: Enemies surrounded them.",
    'The first step is to - Flabby phrase. Use first, or start by instead. Ex: The first step is to realize you have a problem. Better: Start by realizing you have a problem.',
    'Then - When used to describe a sequence of events, stronger transitions exist. Rewrite the transition through action. Ex: He lit a cigarette. Then he made circles with the smoke. Better: He lit a  cigarette and made circles with the smoke.',
    "Take alook at - Redundant phrase & Nominalization. You don't need take a. Ex: Take a look at this photo. Better: Look at this photo.",
    'Take action - Flabby verb construction. Use act instead. Ex: You must take action to resolve the matter now. Better: You must act to resolve the matter now.',
    'Takes up - Flabby verb construction. Use consume instead. Ex: If blog chores take up too much of your time, outsource them. Better: If blog chores consume too much of your time, outsource them.',
    'Taking up - Flabby verb construction. Use consume instead. Ex: If blog chores take up too much of your time, outsource them. Better: If blog chores consume too much of your time, outsource them.',
    "Talk about - Flabby verb construction. Use discuss instead. Ex: Let's talk about it. Better: Let's discuss it.",
    'Tall - Weak Adjective. Replace with something more precise. Ex: The building is tall. Better: The building is six hundred feet tall.',
    "Temper tantrum - Redundant phrase. You don't need temper. Ex: The kid is having a temper tantrum. Better: The kid is having a tantrum.",
    'The most important thing is to - Flabby expression. Delete it. Ex: The most important thing is to remain positive. Better: Remain positive.',
    'The important thing is to - Flabby expression. Delete it. Ex: The most important thing is to remain positive. Better: Remain positive.',
    "The reason - Flabby phrase. Delete it. Ex: The reason you hate me is because I'm beautiful. Better: You hate me because I'm beautiful.",
    "There's  - Grammar expletive that robs your sentence of strength. Avoid it. Ex: There's time to change your mind. Better: You have time to change your mind.",
    "There is - Grammar expletive that robs your sentence of strength. Avoid it. Ex: There's time to change your mind. Better: You have time to change your mind.",
    'There are - Grammar expletive that robs your sentence of strength. Avoid it. Ex: There are some bloggers who seem to have all the luck. Better: Some bloggers seem to have all the luck.',
    'There was - Grammar expletive that robs your sentence of strength. Avoid it. Ex: There was a logger who seemed to have all the luck. Better: A blogger seemed to have all the luck.',
    'There were - Grammar expletive that robs your sentence of strength. Avoid it. Ex: There are some bloggers who seem to have all the luck. Better: Some bloggers seem to have all the luck.',
    'There will be - Grammar expletive that robs your sentence of strength. Avoid it. Ex: There will be some people who fail the class. Better: Some people will fail the class.',
    'Thing - Vague and weak word. Replace "thing" with whatever you are taking about. Ex: The thing that gets me should alert you to the problem. Better: I get disturbed and assume you should be alerted to the problem.',
    'Things - Weak work. Provide a better description rather than an indefiinite pronoun. Ex: Things better improve around this house. Better: Specific chores need improvement.',
    'This is a (insert noun here) that - Flabby construction. Use this (insert noun here). Ex: This is a subject that students love. Better: Students love this subject.',
    'Time and time again - Flabby phrase. Use repeatedly instead. Ex: You will see it time and time again. Better: You will see it repeatedly.',
    'Took up - Flabby phrase. Use consumed or occupied instead. Ex: It took up all my time and energy. Better: It consumed all my time and energy.',
    "Totally - This words doesn't add to the sentence and can often be removed. Ex: I totally get your idea. Better: I get your idea.",
    'Transformation - Nominalization (wordiness introduced when someone uses the noun equivalent of a verb or adjective). Ex: His transformation into an athlete caused shock among his peers. Better: He transformed into an athlete and shocked his peers.',
    'Try to figure out - Flabby phrase. Use determine, guess or decide instead. Ex: Try to figure out what you want in life. Better: Decide what you want in life.',
    "Two equal halves - Redundant phrase. You don't need two equal. Ex: Cut the fruit in two equal halves. Better: Cut the fruit in halves.",
    'Utilize - Use simpler replacement, such as use. Ex: Utilize your time wisely. Better: Use your time wisely.',
    'Very - Flabby modifier. Use a stronger word that very is modifying. Ex: I was very scared. Better: I was petrified.',
    'Went back over - Flabby phrase. Use reread or reevaluated instead. Ex: They went back over the case files. Better: They reread the case files.',
    'When it comes to - Flabby phrase. Use when, with or delete the phrase instead. Ex: When it comes to creating blog posts, you must choose headlines wisely. Better: When creating blog posts, you must choose headlines wisely. Best: Choose headlines wisely when you create a blog post.',
    'Which is - Flabby phrase you can live without. Ex: Chocolate, which is my favorite flavor, is also the name of my cat. Better: Chocolate, my favorite flavor, is also the name of my cat.',
    'Who is - Flabby phrase you can live without. Ex: His brother, who is a doctor, lives in Washington. Better: His brother, the doctor, lives in Washington.',
    'Will be different - Flabby "to be" verb construction. Revise. Ex: Each instance will be different. Better: Each instance will differ.',
    "Within that time frame - Redundant phrase. You don't need frame. Ex: You must sign the paperwork within that time frame. Better: You must sign the paperwork within that time.",
    "With reference to - Flabby phrase. Use regarding instead. Ex: With reference to what you said earlier, I don't agree. Better: Regarding what you said earlier, I don't agree.",
    "Write down - Redundant phrase. You don't need down. Ex: Write down your name on this sheet of paper. Better: Write your name on this sheet of paper.",
    'You can - Flabby verb helpers. Delete or revise. Ex: You can visit Oz by following the Yellow Brick Road. Better: To visit Oz, follow the Yellow Brick Road.',
    "You're going to - Flabby phrase. Use you'll instead. Ex: You're going to learn about writing in class today. Better: You'll learn about writing in class today.",
    "You're going to have to - Flabby phrase. Use you'll have to, or you must instead. Ex: You're going to need to exercise each day. Better: You'll need to exercise each day.",
    "You're going to need to - Flabby phrase. Use you'll have to, or you must instead. Ex: You're going to need to exercise each day. Better: You'll need to exercise each day.",
]

if __name__ == "__main__":
    sys.exit(main(sys.argv))
