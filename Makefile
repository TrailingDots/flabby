#
# Makefile for flabby
#
DESTDIR=/
PROJECT=my_flabby
BUILDDIR=$(CURDIR)/$(PROJECT)
LIBDIR=$(CURDIR)/$(PROJECT)
TOOLSDIR=$(CURDIR)/$(PROJECT)
LOCALDIR=$(HOME)/.local

PYTHON=`which python`

# Coverage requires special handling
COVERAGE=`which coverage`

PYTEST=`which pytest`

RM=/usr/bin/rm

RM=/usr/bin/rm
CP=/usr/bin/cp

all:
	make help

help:
	@echo "make help     - This help message."
	@echo "make build    - Build source distributable package. Test locally"
	@echo "make coverage - Run test suite. Capture with 'script' command."
	@echo "make tests    - Run tests only."
	@echo "make install  - install on local system"
	@echo "make backup   - Create tgz backup one dir above this dir."
	@echo "make backup_git   - Git notes. Create tgz backup one dir above this dir."
	@echo "make wc       - Perform word count for line counts."
	@echo "make clean    - Get rid of scratch files"
	@echo "make doc      - Create html file from README.md"
	@echo "make lint     - Perform extensive lint with prospector"

build:
	python setup.py sdist

coverage:
	$(COVERAGE) run --branch -m unittest
	$(COVERAGE) report
	$(COVERAGE) html
	@echo
	@echo ============================================================
	@echo To view coverage load: file://$(PWD)/htmlcov/index.html
	@echo ============================================================

install:
	@echo Dummied out. Users must write their own install scripts.
	@echo $(PYTHON) setup.py install --root $(DESTDIR) $(COMPILE)

wc:
	./wc.sh

backup:
	./backup.sh

tests:
	${PYTHON} ${PYTEST}

# Users may not have all these nagging linters, but that's OK.
# Do nothing is radon cannot be found.
lint:
	@echo "Cyclomatic Complexity"
	radon cc $$(find . -name '*.py') --total-average --show-complexity
	@echo ""
	@echo "Maintainability Index"
	radon mi $$(find . -name '*.py') --show 
	@echo ""
	@echo "Prospector"
	prospector


# A failure status will stop the make. The code below
# gets around this and simply continues and ignores a failure.
# A "failure" does not imply bad code, but that the 
# utility detected errors in your code.
linters:
	@echo
	@echo "============= flake8 =================="
	flake8                                || echo "flake8 failed"
	@echo
	@echo "============= pylint =================="
	pylint        $$(find . -name '*.py') || echo "pylint failed"


# Use pandoc for formating md -> html.
doc:
	pandoc -s  README.md -o README.html
	@echo "Load the html file into your browser with this URL:"
	@echo "    file://$(PWD)/README.html"

# Due to difficulties in creating a package, 
# kludge cleaning out the code to track difficulties 
# and installing in various ways.
clean:
	find . -name logs.log -delete
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete
	$(PYTHON) setup.py clean

        
