# Flabby as a Writing Tool

Writing implies a certain amount of knowledge
and writing expertise on your part. While you may not consider
yourself as the world's greatest writer, your past
experience knows you can learn more about your writing.

You know you can certainly polish you writings.

You want to keep reader's attention and keep them
coming back.

Each sentence you write reflects your abilties
to attract and keep that taut chain connecting
your writing to your conclusion.

Your writing style strengthens or weakens on
that connection.

Your words weave a crisp and clear web of ideas that
support that writing's headline. You take your craft
seriously to gleen all you can from that web.

You've struggeled to commit to create a great
writing topic and now you think the writing reflects
your thoughts. Your edits reflect the shine and
polish of these efforts.

What if that is not enouth?

What about weaknesses remaining not easily spotted?
Those weaknesses hiding in plain sight?

You can always edit more. But how much effort should
you expend for problems that can reveal themself through
an automated filter?

You can always read more about using certain words
or phrases that change the strength of your ideas.
The internet has many blogs on this topic.

But certain words and phrases could slide through the editing
phase as silently as a ghost in the night.
These phrases dilute reader's attention ever so little.

This utility highlights these seemingly 
benign words and phrases that erode your writing.
They just add unnecessary flab to your writing.

Just reading a list will not banish these slippery
flabs. You need to use this utility over and over
until it becomes "in your bones".

# Target Audience 

Because this preliminary release provides only a
command line interface and requires python3 and
linux knowledge to run, only people with this
prerequisite knowledge should use this code.

Other than having python 3 installed on a linux
systems, only very elementary shills with linux are required.

This code runs on any recent OSX or linux system with the
listed utilities.

Because technical documentation seeks to provide clarity
with minimal explanation, the use of weak, vague or
ambiguous words and phrases can muddy an otherwise
decent software project.

This utility targets developers and technical software
documentation writers. Any writer may benefit
from the information produced by this utility.

# Installing

Flabby lives on your execution path, $PATH.
Almost all users have $HOME/bin that holds various 
personal utilities and executables.

Install flabby so you can run it from anywhere:

~~~ bash
    # Download the GitLab repo of Flabby
    git clone https://gitlab.com/TrailingDots/flabby.git
    cd flabby
    # Copy flabby to a location in your path
    cp flabby/flabby.py $HOME/bin
    # Try out flabby in your home dir
    cd
    # Run with no options - will produce simple help message
    flabby 
    usage: Flabby writing utility [-h] -i INPUT [-o OUTPUT]
    Flabby writing utility: error: the following arguments are required: -i/--input
~~~

The above procedure results in a single file, flabby, in
as an executable file. This file, flabby, may then be
executed anywhere on you sytem.

# Features

1. Extensible dictionary of words and phrases.
1. Examples of flabby phrases compared to better wording.
1. 300+ flabby words and phrases with examples and improvements.

# False Positives

Flabby does not know the context of the phrases. It
merely triggers on the presense of the word or
phrase when scanning the input.

For example, "about" uses the numerical context but
ignores other valid uses of about. It will warn you
about such useage as: "I don't now what this is about".

Use your common sense.

# Extending the Phrases Dictionary

Not everyone will agree with the exisiting flabby phrases.
Many people will wish to add their own ideas. Flabby
can accomodate adding and deleting phrases, reasons, and
examples of alternative usage.

At the end of the flabby code all the words and phrases flabby
knows about get listed.

To add your own phrases simply insert a line into this
flabby list.

Please retain the alphabetical ordering of
phrases in case you need to check or modify a phrase later.

Flabby uses a trivial format for phrase entries. The first
entry illustrates this:

~~~ text
    'About - Try not to use this term when discussing quantities. Use approximately or a range instead. Ex: About 20 people attended. Better: Approximately 20 people attended. Or: Fifteen to twenty people attended.',
~~~

Follow this format:

* Each entry is on a *single* line. This makes the line rather long but
  simplifies internal operations.
* Four spaces. These spaces highlight the start of an entry.
* A single quote to start the entry See below if the entry contains
  embedded single quotes.
* The word or phrase considered flabby. Please capitalize the first word as
this allows an easy way to find it in the phrases list.
* 'space minus space': A separator between the phrase and an explanation.
* An explanatory sentence of the reason this phrase get classified
as flabby.
* Ex: An example of using this flabby phrase.
* Better: A suggestion of a better way to use this phrase.
* A comma. This concludes the phrase and allows it to become part of the
  phrase dictionary.

The Ex: and Better: pair may repeat multiple times to more fully illustrate
and explain the phrase's usage. At least one Ex:Better: pair
must exist. Using an Ex: implies a Better: follows.

The phrase "Are after" illustrates multiple uses of Ex:Better:

~~~ text
    "Are after - Clunky verb construction. Use follow, or seek, or desire, or want. Ex: The events are after the lecture. Better: The events follow the lecture. Ex: I don't know what you are after. Better: I don't know what you want.",
~~~

## Testing Your Changes

Googling "weak phrases" provides abundant material for extending
the flabby dictionary. After adding you favorite entries, you must
test them. I suggest you test each phrase immediately after
their addition.

For example, suppose you added 'Personally' to the dictionary.

~~~ text
    "Personally - Redundant unless used for emphasis. Ex: Personally I don't this this is a problem. Better: I don't think this is a problem.",
~~~

This is also an example of using double quotes to bound the entry.
The presence of a single quote in "don't" demands signe quotes.

Create a new file in some directory using the word "personally".
Call the file "test.txt" or some other throw-away name:

~~~ text
    Personally I don't care.
~~~

Now run `flabby test.txt`:

~~~ text
    flabby --input test.txt
    1: Personally I don't care.  
        ==> personally <==
        Example: Personally I don't think this is a problem.
        Better: I don't think this is a problem.

~~~

## Single and Double Quotes

A phrase containing single or double quotes needs special
attention. For example, here are two entries that contain
each kind of quotes:

~~~ text
    'You can - Flabby verb helpers. Delete or revise. Ex: You can visit Oz by following the Yellow Brick Road. Better: To visit Oz, follow the Yellow Brick Road.',
    "You're going to - Flabby phrase. Use you'll instead. Ex: You're going to learn about writing in class today. Better: You'll learn about writing in class today.",
~~~

The first entry does not contain a single internal quote.
By default this line gets bounded by single quotes.

The second entry uses `you'll` with a single quote. Flabby then
uses double quotes for the entire line.

# Command Line Useage

Flabby currently runs as a command line utility.

~~~ text
    flabby --help
    usage: Flabby writing utility [-h] -i INPUT [-o OUTPUT]

    optional arguments:
    -h, --help            show this help message and exit
    -i INPUT, --input INPUT
                          User document to scan for flabbiness.
    -o OUTPUT, --output OUTPUT
                          Output file of results.

    Usage Examples:
        ./flabby --help
        ./flabby --input MyBlog.md
        ./flabby -i great_blog.txt
        ./flabby --input my_blog.md --output my_blog.out
        ./flabby --input my_blog.md -o my_blog.out
~~~

# TODO

1. Convert Flabby to a web based utililty. This involves a server and a UI.
1. Add interactive changes while in the browser.
1. More? Depending upon popularity, these will be implemented.

# Links

[297 Flabby Words and Phrases that Rob your Writing of All Its Power]( https://smartblogger.com/weak-writing/)

Google "weak words in writing" for many references to additional
ideas to strengthen your writing.

# License

See LICENSE file.

